<?php

/*
 * Instantiates and returns a SpinPapiData using the given configuration.
 */

// Configure your station's time zone here. http://php.net/manual/en/timezones.php
date_default_timezone_set('America/New_York');

require_once(dirname(__FILE__) . '/SpinPapiClient.inc.php');
require_once(dirname(__FILE__) . '/SpinPapiData.php');

// Set your configuration parameters here.
return new SpinPapiData(
    array(
        // Your station's ID in Spinitron.
        'station' => 'wzbc',

        // Your SpinPapi authentication credentials. Ask Spinitron if you don't have yet.
        'userId' => 'myuserid',
        'secret' => 'mysecret',

        // To use a local file cache, set to the (string) file path of the
        // cache directory, which must be writable by the webserver process.
        // For a memcache, set to an array of one or more memcache servers,
        // see http://au1.php.net/manual/en/memcached.addservers.php
        'cache' => dirname(__FILE__) . '/../cache',

        /*
         * What to do on SpinPapi error response. Set to:
         *   - 'notice' to trigger a PHP E_USER_NOTICE
         *   - 'throw' to throw a SpinPapiException
         *   - 'log:filepath' to write a message to a log file at filepath
         */
        'onError' => 'log:' . dirname(__FILE__) . '/../cache/spd.log',
    )
);
