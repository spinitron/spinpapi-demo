<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>SpinPapi API demo</title>
    <meta name="Description" content="Demonstrates using the SpinPapi API with PHP and AJAX refresh.">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto+Condensed:300">
</head>

<body class="normal">
<header>
    <nav>
        <ul>
            <li><a href="//spinitron.bitbucket.io/">Basic iframe</a></li>
            <li><a href="//spinitron.bitbucket.io/schedule.html">Schedule</a></li>
            <li><a href="//spinitron.bitbucket.io/current.html">Current playlist</a></li>
            <li><a href="//spinitron.bitbucket.io/headline.html">Headline</a></li>
            <li><a href="//spinitron.bitbucket.io/widget.html">JS Widget</a></li>
            <li><a href="/demo/www/">SpinPapi API</a></li>
        </ul>
    </nav>
</header>

<div id="wrapper" class="clear">
    <div class="widget-box">
        <h1>Today on WZBC</h1>
        <h2>Recent spins</h2>
        <div id="spin-recent"><?php include('recent.php') ?></div>
        <h2>Coming up later today</h2>
        <div id="spin-shows"><?php include('today.php') ?></div>
    </div>

    <div class="main-text">
        <h1>Access SpinPapi via PHP and JavaScript</h1>

        <p>This page shows how to use data from Spinitron's SpinPapi API in a web page
            using PHP directly or using JavaScript and Ajax to update a page automatically
            from the browser.</p>

        <p>In the yellow box, the recent spins and upcoming shows use data obtained
            using SpinPapi. The markup for recent spins and upcoming shows is placed
            on the page by server-side PHP scripts (partial views, see below) on page
            load. Additionally, the display of recent spins is refreshed every five
            seconds by a client-side JavaScript using an Ajax request that obtains
            new markup from one of these partial views.</p>

        <h2>Overview</h2>

        <p>The demo is available in Bitbucket as a
            <a href="https://bitbucket.org/spinitron/spinpapi-demo/src">git repository</a> or a
            <a href="https://bitbucket.org/spinitron/spinpapi-demo/downloads">ZIP file to download</a>.
            The files in the demo are organized as follows:</p>

    <pre>
    |--app/
    |   |--SpinPapiClient.inc.php
    |   |--SpinPapiData.php
    |   |--getSpd.php
    |--cache/
    |--www/
        |--index.php
        |--recent.php
        |--today.php
        |--styles.css</pre>

        <ul>
            <li><code>app</code> contains the software the web page uses to access SpinPapi.</li>
            <li><code>cache</code> holds data files comprising <code>SpinPapiClient</code>'s cache.</li>
            <li><code>www</code> has this web page, its style sheet and the two partial
                views <code>recent.php</code> and <code>today.php</code>.
            </li>
        </ul>

        <h2>Try it yourself</h2>

        <p>If you have a computer with PHP, you can try the demo as follows:</p>
        <ul>
            <li><a href="https://bitbucket.org/spinitron/spinpapi-demo/downloads">Download</a>
                the demo zip file
            </li>
            <li>Unzip the demo into an empty directory.</li>
            <li>Edit <code>app/getSpd.php</code>. Configure your station and SpinPapi credentials.</li>
            <li>Open a command shell in the <code>www</code> directory and start a test PHP web server:
        <pre>
 <code>php -S localhost:8000</code></pre>
            </li>
            <li>Navigate to <code>http://localhost:8000/</code> in your web browser.</li>
        </ul>

        <p>To use this approach in your web site, put the <code>app</code> and <code>cache</code> directories
            outside your web server's document root and follow the example code in the <code>www</code>
            directory in your web pages. You may need to change the include path to <code>getSpd.php</code>
            in the partial views.
        </p>

        <h2>How it works</h2>

        <p>Study the <code>index.php</code> file, i.e. the markup for this page.
            It is ordinary HTML with only two interesting parts. First,
            inside the <code>&lt;div class="widget-box"&gt;</code> block are two PHP blocks that include the
            output of the <code>recent.php</code> and <code>today.php</code> partial views, of which more below.
            Second, at the bottom there is
            a JavaScript that, using <a href="http://api.jquery.com/load/">jQuery's <code>load()</code></a>
            Ajax function, replaces the content of the
            <code>&lt;div id="spin-recent"&gt;</code> block every five seconds. The page is no more fancy than that.</p>

        <p>The two PHP scripts <code>recent.php</code> and <code>today.php</code> are known as <em>partial views</em>
            in the <a href="https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93controller">MVC world</a>.
            A partial view is a script that generates and outputs a fragment of a web page. It should be fairly
            clear from reading
            these files that <code>recent.php</code> uses a
            <a href="http://php.net/manual/en/control-structures.alternative-syntax.php">PHP <code>foreach</code></a>
            loop to generate a number of
            <code>&lt;p&gt;</code> elements, each containing text describing a recent spin.
            <code>today.php</code> is similar with each <code>&lt;p&gt;</code> element containing
            the time and name of a scheduled show. However, <code>today.php</code> has additional code to sort the array
            of
            shows received from SpinPapi and then select the ones to display.</p>

        <p>If you have the test web server running, as described above, you can view the output of these partial views
            in your browser by navigating to and viewing the page source of the following URLs:

    <pre>
    <code>http://localhost:8000/recent.php</code>
    <code>http://localhost:8000/today.php</code></pre>

        <p>Both partial views use the following line to bootstrap the <code>SpinPapiData</code>
            and <code>SpinPapiClient</code> objects. </p>

    <pre>
    <code>$spData = include(dirname(__FILE__) . '/../app/getSpd.php');</code></pre>

        <p>The <code>getSpd.php</code> script instantiates (with your configuration) a new
            <code>SpinPapiData</code> object that the partial view saves into its <code>$spData</code>
            variable. This <code>SpinPapiData</code> object offers very simple methods to access
            data from SpinPapi while concealing the details of the API, caching and configuration. For
            example, the <code>recent.php</code> partial view fetches the most recent three songs from
            Spinitron with the expression <code>$spData->songs(3)</code>. The code in <code>today.php</code>
            to fetch the upcoming schedule is similar.
            <code>SpinPapiData</code> is documented in its class file using conventional docblocks.
            Refer to the <a href="https://bitbucket.org/spinitron/documentation">SpinPapi Specification</a> (in
            Bitbucket)
            for the full range of SpinPapi's methods, parameters and responses.
        </p>

        <p>Feel free
            to <a href="http://php.net/manual/en/keyword.extends.php">extend</a> <code>SpinPapiData</code>
            and add or <a href="http://php.net/manual/en/language.oop5.inheritance.php">override</a> methods
            to meet your particular needs, or just use it as-is. <code>SpinPapiClient</code> is considerably
            more complex and we don't recommend you concern yourself with its workings unless you really want to. If
            it doesn't meet your needs, let us know.</p>


        <h3>To summarize</h3>

        <p>The two partial views <code>recent.php</code> and <code>today.php</code>:</p>
        <ol>
            <li>load <code>getSpd.php</code>, which returns a <code>SpinPapiData</code> object,</li>
            <li>use the <code>SpinPapiData</code> object's methods to access data in Spinitron,</li>
            <li>generate fragments of HTML as output.</li>
        </ol>
        <p>The main web page <code>index.php</code>:</p>
        <ol>
            <li>on initial page load, inserts the HTML fragments output by the partial views into the page using PHP
                <code>include()</code>,
            </li>
            <li>initializes a JavaScript in the client's web browser that uses
                <a href="http://api.jquery.com/load/">jQuery's <code>load()</code></a> function to
                replace the list of recent spins with freshly loaded output of <code>recent.php</code>
                every five seconds.
            </li>
        </ol>

        <h2>In closing</h2>

        <p>Spinitron's terms of service require that you have access credentials in order to use SpinPapi
            and that you don't share them with other entities. We also require that, when you use SpinPapi
            to access data that you use in a web site or mobile app (etc.), you use a local cache. This
            demo uses a file-based cache. The <code>SpinPapiClient</code> supports also caching with
            <a href="http://memcached.org/"><code>Memcached</code></a>.
        </p>

        <p>As always, for support contact Eva at Spinitron on 617 233 3115 or by email at
            <a href="mailto:eva@spinitron.com">eva@spinitron.com</a>.</p>
    </div>
</div>

<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script>
    jQuery(function ($) {
        setInterval(
            function () {
                $('#spin-recent').load('recent.php');
            },
            5000
        );
    });
</script>
</body>
</html>
