<?php

/** @var SpinPapiData */
$spData = include(dirname(__FILE__) . '/../app/getSpd.php');

// Fetch all scheduled shows for "today" starting at the top of the last hour. See the SpinPapi spec.
$hour = date('H');
$shows = $spData->regularShowsInfo('today', $hour);

if ($shows === false) {
    return '';
}

// Sort the shows received from SpinPapi by on-air time.
usort(
    $shows,
    function ($a, $b) {
        return strcmp($a['OnairTime'], $b['OnairTime']);
    }
);

// Select the shows with on-air time in the next 6 hours.
$coming = array();
for ($h = $hour; $h <= $hour + 6 || count($coming) < 3; $h += 1) {
    foreach ($shows as $show) {
        if (substr($show['OnairTime'], 0, 2) == $h % 24) {
            $coming[] = $show;
        }
    }
}

?>

<?php foreach ($coming as $show): ?>
    <p><?= substr($show['OnairTime'], 0, 5) ?>
        <b><?= $show['ShowName'] ?></b>
        with <?= $show['ShowUsers'][0]['DJName'] ?></p>
<?php endforeach ?>
