<?php

/** @var SpinPapiData */
$spData = include(dirname(__FILE__) . '/../app/getSpd.php');
$songs = $spData->songs(3);

if ($songs === false) {
    return '';
}

?>

<?php foreach ($songs as $song): ?>
    <p><?= substr($song['Timestamp'], 0, 5) ?>
        <b><?= $song['ArtistName'] ?></b>
        <em>“<?= $song['SongName'] ?>”</em>
        from <?= $song['DiskName'] ?>
    </p>
<?php endforeach ?>

<p><small>(Updated <?= date('H:i:s') ?>)</small>